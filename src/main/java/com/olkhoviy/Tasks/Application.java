package com.olkhoviy.Tasks;

public class Application {
    public static void main(String[] args) {
        int[] firstArray = new int[]{1, 4, 6, 7, 12, 15, 12, 14, 11, 0, 9};
        int[] secondArray = new int[]{1, 3, 2, 9, 67, 32, 11, 12, 14, 0, 3};
        FirstTask.makeAnotherArray(firstArray, secondArray);
        SecondTask.deleteRepeat(firstArray);
        ThirdTask.findSeries(firstArray);
    }
}
