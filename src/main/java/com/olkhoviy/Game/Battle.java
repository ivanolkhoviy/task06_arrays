package com.olkhoviy.Game;

import java.util.Random;

class Battle {
    int Monster(int heroPower) {
        int monsterPower ;
        Random random = new Random();
        monsterPower = random.nextInt(100 + 5);
        System.out.println("You find monster with power =");
        System.out.println(monsterPower);
        if (heroPower > monsterPower || heroPower == monsterPower) {
            System.out.println("You win this battle");
            heroPower = heroPower - monsterPower;

        } else {
            System.out.println("You lose this battle");
            heroPower = 0;
        }

        return heroPower;
    }
}
