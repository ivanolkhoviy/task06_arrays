package com.olkhoviy.Game;

public class Application {
        public static void main(String[] args) {
            HeroPower hero = new HeroPower();
            Battle monster = new Battle();
            DoorGenerator game = new DoorGenerator();
            game.generate(hero,monster);
        }
    }
