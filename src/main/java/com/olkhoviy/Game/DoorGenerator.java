package com.olkhoviy.Game;

import java.util.Random;
import java.util.Scanner;

public class DoorGenerator {

    private int heroPower = 25;
    private int[] door = new int[10];

    public int generate(HeroPower hero, Battle monster) {


        System.out.println("Set the door you want to come from 1 to 10");
        Scanner scanner = new Scanner("UTF-8");
        Random random = new Random();
        int x = scanner.nextInt();
        if (1 > x || x > 10) {
            System.out.println("You write incorrect number");
            return 1;

        }
        for (int i = 0; i < 10; i++) {
            door[i] = random.nextInt(2 + 1);


            switch (door[i]) {
                case 1:
                    return hero.artifact(heroPower);

                case 2:
                    return monster.Monster(heroPower);

                case 3:
                    return 1;

                default:
                    throw new IllegalStateException("Unexpected value: " + door[i]);
            }


        }

        return 1;
    }
}
